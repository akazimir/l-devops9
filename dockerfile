FROM busybox
MAINTAINER Anna Kazimirova Kolesarova
ENTRYPOINT ["/bin/cat"]
CMD ["/etc/passwd"]
